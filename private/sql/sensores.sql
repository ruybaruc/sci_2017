-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-04-2017 a las 08:02:43
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `invernadero`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sensores`
--

CREATE TABLE `sensores` (
  `idsens` int(11) NOT NULL,
  `tstamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `temp` int(11) DEFAULT NULL,
  `temp1` int(11) DEFAULT NULL,
  `bomba` int(11) DEFAULT NULL,
  `humedad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sensores`
--

INSERT INTO `sensores` (`idsens`, `tstamp`, `temp`, `temp1`, `bomba`, `humedad`) VALUES
(1, '2017-04-08 00:40:33', 25, 23, 1, 66),
(2, '2017-04-08 00:40:33', 3, 0, 0, 1),
(3, '2017-04-08 00:40:33', 3, 0, 0, 1),
(4, '2017-04-08 00:40:33', 3, 0, 0, 1),
(5, '2017-04-08 00:40:33', 3, 0, 0, 1),
(6, '2017-04-08 00:40:33', 3, 0, 0, 1),
(7, '2017-04-08 00:40:33', 3, 0, 0, 1),
(8, '2017-04-08 00:40:33', 3, 0, 0, 1),
(9, '2017-04-08 00:40:33', 3, 0, 0, 1),
(10, '2017-04-08 00:40:33', 3, 0, 0, 1),
(11, '2017-04-08 00:40:57', 25, 0, 0, 70),
(12, '2017-04-08 00:42:01', 25, NULL, NULL, 70),
(13, '2017-04-08 00:58:48', 26, NULL, NULL, 71),
(14, '2017-04-08 00:58:49', 27, NULL, NULL, 72),
(15, '2017-04-08 00:58:51', 28, NULL, NULL, 73),
(16, '2017-04-08 00:58:52', 29, NULL, NULL, 74),
(17, '2017-04-08 00:58:54', 30, NULL, NULL, 75),
(18, '2017-04-08 00:58:56', 31, NULL, NULL, 76),
(19, '2017-04-08 00:58:57', 32, NULL, NULL, 77),
(20, '2017-04-08 00:58:59', 33, NULL, NULL, 78),
(21, '2017-04-08 00:59:01', 34, NULL, NULL, 79),
(22, '2017-04-08 00:59:02', 35, NULL, NULL, 80),
(23, '2017-04-08 00:59:04', 36, NULL, NULL, 81),
(24, '2017-04-08 00:59:05', 37, NULL, NULL, 82),
(25, '2017-04-08 00:59:07', 38, NULL, NULL, 83),
(26, '2017-04-08 00:59:08', 39, NULL, NULL, 84),
(27, '2017-04-08 00:59:10', 40, NULL, NULL, 85),
(28, '2017-04-08 00:59:11', 41, NULL, NULL, 86),
(29, '2017-04-08 00:59:13', 42, NULL, NULL, 87),
(30, '2017-04-27 05:44:42', 43, NULL, NULL, 88),
(31, '2017-04-27 05:44:44', 44, NULL, NULL, 89),
(32, '2017-04-27 05:44:45', 45, NULL, NULL, 90),
(33, '2017-04-27 05:44:47', 46, NULL, NULL, 91),
(34, '2017-04-27 05:44:49', 47, NULL, NULL, 92),
(35, '2017-04-27 05:44:51', 48, NULL, NULL, 93),
(36, '2017-04-27 05:44:52', 49, NULL, NULL, 94),
(37, '2017-04-27 05:44:54', 50, NULL, NULL, 95),
(38, '2017-04-27 05:44:56', 51, NULL, NULL, 96),
(39, '2017-04-27 05:44:58', 52, NULL, NULL, 97),
(40, '2017-04-27 05:45:00', 53, NULL, NULL, 98),
(41, '2017-04-27 05:45:01', 54, NULL, NULL, 99),
(42, '2017-04-27 05:45:03', 55, NULL, NULL, 100),
(43, '2017-04-27 05:45:05', 56, NULL, NULL, 101),
(44, '2017-04-27 05:45:06', 57, NULL, NULL, 102),
(45, '2017-04-27 05:45:08', 58, NULL, NULL, 103);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `sensores`
--
ALTER TABLE `sensores`
  ADD PRIMARY KEY (`idsens`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `sensores`
--
ALTER TABLE `sensores`
  MODIFY `idsens` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
