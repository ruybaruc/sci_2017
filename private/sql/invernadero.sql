-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-04-2017 a las 07:51:53
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `invernadero`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sensores`
--

CREATE TABLE `sensores` (
  `idsens` int(11) NOT NULL,
  `tstamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `temp` int(11) DEFAULT NULL,
  `temp1` int(11) DEFAULT NULL,
  `bomba` int(11) DEFAULT NULL,
  `humedad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sensores`
--

INSERT INTO `sensores` (`idsens`, `tstamp`, `temp`, `temp1`, `bomba`, `humedad`) VALUES
(1, '2017-04-08 00:40:33', 25, 23, 1, 66),
(2, '2017-04-08 00:40:33', 3, 0, 0, 1),
(3, '2017-04-08 00:40:33', 3, 0, 0, 1),
(4, '2017-04-08 00:40:33', 3, 0, 0, 1),
(5, '2017-04-08 00:40:33', 3, 0, 0, 1),
(6, '2017-04-08 00:40:33', 3, 0, 0, 1),
(7, '2017-04-08 00:40:33', 3, 0, 0, 1),
(8, '2017-04-08 00:40:33', 3, 0, 0, 1),
(9, '2017-04-08 00:40:33', 3, 0, 0, 1),
(10, '2017-04-08 00:40:33', 3, 0, 0, 1),
(11, '2017-04-08 00:40:57', 25, 0, 0, 70),
(12, '2017-04-08 00:42:01', 25, NULL, NULL, 70),
(13, '2017-04-08 00:58:48', 26, NULL, NULL, 71),
(14, '2017-04-08 00:58:49', 27, NULL, NULL, 72),
(15, '2017-04-08 00:58:51', 28, NULL, NULL, 73),
(16, '2017-04-08 00:58:52', 29, NULL, NULL, 74),
(17, '2017-04-08 00:58:54', 30, NULL, NULL, 75),
(18, '2017-04-08 00:58:56', 31, NULL, NULL, 76),
(19, '2017-04-08 00:58:57', 32, NULL, NULL, 77),
(20, '2017-04-08 00:58:59', 33, NULL, NULL, 78),
(21, '2017-04-08 00:59:01', 34, NULL, NULL, 79),
(22, '2017-04-08 00:59:02', 35, NULL, NULL, 80),
(23, '2017-04-08 00:59:04', 36, NULL, NULL, 81),
(24, '2017-04-08 00:59:05', 37, NULL, NULL, 82),
(25, '2017-04-08 00:59:07', 38, NULL, NULL, 83),
(26, '2017-04-08 00:59:08', 39, NULL, NULL, 84),
(27, '2017-04-08 00:59:10', 40, NULL, NULL, 85),
(28, '2017-04-08 00:59:11', 41, NULL, NULL, 86),
(29, '2017-04-08 00:59:13', 42, NULL, NULL, 87),
(30, '2017-04-27 05:44:42', 43, NULL, NULL, 88),
(31, '2017-04-27 05:44:44', 44, NULL, NULL, 89),
(32, '2017-04-27 05:44:45', 45, NULL, NULL, 90),
(33, '2017-04-27 05:44:47', 46, NULL, NULL, 91),
(34, '2017-04-27 05:44:49', 47, NULL, NULL, 92),
(35, '2017-04-27 05:44:51', 48, NULL, NULL, 93),
(36, '2017-04-27 05:44:52', 49, NULL, NULL, 94),
(37, '2017-04-27 05:44:54', 50, NULL, NULL, 95),
(38, '2017-04-27 05:44:56', 51, NULL, NULL, 96),
(39, '2017-04-27 05:44:58', 52, NULL, NULL, 97),
(40, '2017-04-27 05:45:00', 53, NULL, NULL, 98),
(41, '2017-04-27 05:45:01', 54, NULL, NULL, 99),
(42, '2017-04-27 05:45:03', 55, NULL, NULL, 100),
(43, '2017-04-27 05:45:05', 56, NULL, NULL, 101),
(44, '2017-04-27 05:45:06', 57, NULL, NULL, 102),
(45, '2017-04-27 05:45:08', 58, NULL, NULL, 103),
(46, '2017-04-27 06:17:00', 59, NULL, NULL, 104),
(47, '2017-04-27 06:17:02', 60, NULL, NULL, 105),
(48, '2017-04-27 06:17:03', 61, NULL, NULL, 106),
(49, '2017-04-27 06:17:05', 62, NULL, NULL, 107),
(50, '2017-04-27 06:17:06', 63, NULL, NULL, 108),
(51, '2017-04-27 06:17:08', 64, NULL, NULL, 109),
(52, '2017-04-27 06:17:10', 65, NULL, NULL, 110),
(53, '2017-04-27 06:17:11', 66, NULL, NULL, 111),
(54, '2017-04-27 06:17:13', 67, NULL, NULL, 112),
(55, '2017-04-27 06:17:15', 68, NULL, NULL, 113),
(56, '2017-04-27 06:17:15', 69, NULL, NULL, 114),
(57, '2017-04-27 06:17:17', 69, NULL, NULL, 114),
(58, '2017-04-27 06:37:48', 70, NULL, NULL, 115),
(59, '2017-04-27 06:37:49', 71, NULL, NULL, 116),
(60, '2017-04-27 06:37:51', 72, NULL, NULL, 117),
(61, '2017-04-27 06:37:52', 73, NULL, NULL, 118),
(62, '2017-04-27 06:37:54', 74, NULL, NULL, 119),
(63, '2017-04-27 06:37:55', 75, NULL, NULL, 120),
(64, '2017-04-27 06:37:57', 76, NULL, NULL, 121),
(65, '2017-04-27 06:37:58', 77, NULL, NULL, 122),
(66, '2017-04-27 06:38:00', 78, NULL, NULL, 123),
(67, '2017-04-27 06:38:01', 79, NULL, NULL, 124),
(68, '2017-04-27 06:38:03', 80, NULL, NULL, 125),
(69, '2017-04-27 06:38:04', 81, NULL, NULL, 126),
(70, '2017-04-27 06:38:06', 82, NULL, NULL, 127),
(71, '2017-04-27 06:38:07', 83, NULL, NULL, 128),
(72, '2017-04-27 06:38:09', 84, NULL, NULL, 129),
(73, '2017-04-27 06:38:10', 85, NULL, NULL, 130),
(74, '2017-04-27 06:38:12', 86, NULL, NULL, 131),
(75, '2017-04-28 03:08:31', 87, NULL, NULL, 132),
(76, '2017-04-28 03:08:32', 88, NULL, NULL, 133),
(77, '2017-04-28 03:08:34', 89, NULL, NULL, 134),
(78, '2017-04-28 03:08:35', 90, NULL, NULL, 135),
(79, '2017-04-28 03:08:37', 91, NULL, NULL, 136),
(80, '2017-04-28 03:08:38', 92, NULL, NULL, 137),
(81, '2017-04-28 05:49:46', 93, NULL, NULL, 138),
(82, '2017-04-28 05:49:47', 94, NULL, NULL, 139),
(83, '2017-04-28 05:49:49', 95, NULL, NULL, 140),
(84, '2017-04-28 05:49:50', 96, NULL, NULL, 141),
(85, '2017-04-28 05:49:52', 97, NULL, NULL, 142),
(86, '2017-04-28 05:49:54', 98, NULL, NULL, 143),
(87, '2017-04-28 05:49:56', 99, NULL, NULL, 144),
(88, '2017-04-28 05:49:57', 100, NULL, NULL, 145);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `passwd` varchar(30) NOT NULL,
  `rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nombre`, `passwd`, `rol`) VALUES
(1, 'admin1', 'admin1', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `sensores`
--
ALTER TABLE `sensores`
  ADD PRIMARY KEY (`idsens`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `sensores`
--
ALTER TABLE `sensores`
  MODIFY `idsens` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
