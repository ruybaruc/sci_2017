<?php
Class Sensores_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function getAllSensores() {
        $query = $this->db->query('SELECT * FROM sensores order by tstamp desc limit 1');
        return $query->result();
    }
    
    public function getReporte() {
        $query = $this->db->query('SELECT * FROM sensores order by tstamp desc limit 50');
        return $query->result_array();
    }
    
    public function add() {
        $this->load->helper('url');
        $sensores = $this->getAllSensores();
        $data = array(
			'temp' => $sensores[0]->temp,
			'temp1' => $sensores[0]->temp1,
			'bomba' => $this->input->post('bomba'),	
			'humedad' => $sensores[0]->humedad		
        );
 		$this->db->insert('sensores', $data);
		return $this->db->insert_id();
    }


}