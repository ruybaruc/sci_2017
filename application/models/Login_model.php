<?php
Class Login_model extends CI_Model
{
 function login($nombre, $pass)
 {
 
   $this->db->select('idusuario, nombre, passwd, rol');
   $this->db->from('usuario');
   $this->db->where('nombre', $nombre);
   $this->db->where('passwd', $pass);
   $this->db->limit(1);

   $query = $this -> db -> get();
   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
}