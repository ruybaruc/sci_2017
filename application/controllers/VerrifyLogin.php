<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class VerifyLogin extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('login_model','',TRUE);
 }

 function index()
 {
   //This method will have the credentials validation
   $this->load->library('form_validation');

   $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
   $this->form_validation->set_rules('contraseña', 'contraseña', 'trim|required|callback_check_database');

   if($this->form_validation->run() == FALSE)
   {
     //Field validation failed.  User redirected to login page
     echo "<script>alert('Nombre o contraseña incorrecto. \\nCompruebe que sean correctos y vuelva a intentarlo.\\nEn caso de haber olvidado sus datos de inicio de sesión, contacte a su administrador');
     window.location = '" . base_url("index.php/login") . "';
     </script>";
     //   redirect('login');
   }
   else
   {
     //Go to private area
          redirect('admin');
   }

 }

 function check_database($password)
 {
   //Field validation succeeded.  Validate against database
   $nombre = $this->input->post('nombre');

   //query the database
   $result = $this->login_model->login($nombre, $password);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'idusuario' => $row->idusuario,
         'nombre' => $row->nombre,
         'contraseña' => $row->contraseña,
         'rol' => $row->rol
       );

       $this->session->set_userdata('logged_in', $sess_array);
     }
     return TRUE;
   }
   else
   {
     $this->form_validation->set_message('check_database', 'Usuario o contraseña inválidos');
     return false;
   }
 }
}
?>