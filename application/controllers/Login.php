<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

 function __construct()
 {
   parent::__construct();

 }

 function index()
 {
     $this->load->library("session");
	if(!$this->session->userdata('logged_in'))
   	{
	   $this->load->helper('form');
	   $this->load->view('login_view');
	}
	else {
		redirect('admin');
	}
 }

 function logout() {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('login', 'refresh');
 }

}

?>