<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sensores extends CI_Controller {

	public function index()
	{	$this->load->model('sensores_model');
		$data['sensores'] = $this->sensores_model->getReporte();
		$this->load->view('reporte', $data);
	}

	public function actualizar(){
		$this->load->model('sensores_model');
		$this->sensores_model->add();
		redirect('admin');
	}

}
