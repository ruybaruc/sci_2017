<!DOCTYPE html>
<html>
  <head>
    <title>Smoothy Responsive Template</title>
    <meta name="keywords" content="" />
	<meta name="description" content="" />
    <!-- 
    Smoothy Template 
    http://www.templatemo.com/tm-396-smoothy
    -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/templatemo_style.css" rel="stylesheet">
   	<link rel="stylesheet" href="<?php echo base_url(); ?>css/templatemo_misc.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/nivo-slider.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/slimbox2.css" type="text/css" media="screen" /> 
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,600' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.min.js"></script>
    <script type="text/JavaScript" src="<?php echo base_url(); ?>js/slimbox2.js"></script> 

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/ddsmoothmenu.css" />
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/ddsmoothmenu.js"></script>

<!--/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

-->


<script type="text/javascript">

ddsmoothmenu.init({
	mainmenuid: "templatemo_flicker", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>

<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function() {   var bombaBtn = document.getElementById("bombabtn");

  bombaBtn.addEventListener("click", function(){var formulario = document.getElementById("myform");
    var fbomba = document.getElementById("bomba");
    var bomba = <?php echo $sensores[0]->bomba; ?>;
      if (bomba) {
        fbomba.value = 0;
      }else{
        fbomba.value = 1;
      };
    formulario.submit();}); }, false); 


</script>

  </head>
  <body>
     <form action="<?php echo base_url('index.php/sensores/actualizar'); ?>" method="post" id="myform">
        <input type="text" name="bomba" id="bomba" style="display: none;">
        <input type="submit" name="ok" value="ok" style="display: none;">
  </form>
    <header>
    <!-- start menu -->
    <div id="templatemo_home">
    	<div class="templatemo_top">
      <div class="container templatemo_container">
        <div class="row">
          <div class="col-sm-3 col-md-3">
            <div class="logo">
              <a href="#"><img src="<?php echo base_url(); ?>images/templatemo_logo.png" alt="smoothy html5 template"></a>
            </div>
          </div>
          <div class="col-sm-9 col-md-9 templatemo_col9">
          	<div id="top-menu">
            <nav class="mainMenu">
              <ul class="nav">
                <li><a class="menu" href="#templatemo_home">Inicio</a></li>
                <li><a class="menu" href="#templatemo_about">Panel</a></li>
                <li><a class="menu" href="<?php echo base_url(); ?>index.php/sensores">Generar Reporte</a></li>
                <li><a class="menu" href="<?php echo base_url(); ?>index.php/login/logout">Salir</a></li>
              </ul>
            </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    <div class="clear"></div>
    <!-- end menu -->
	
		<div  id="slider"  class="nivoSlider templatemo_slider">
        	<a href="#"><img src="<?php echo base_url(); ?>images/slider/img_1_blank.jpg" alt="slide 1" /></a>           	
			<a href="#"><img src="<?php echo base_url(); ?>images/slider/img_2_blank.jpg" alt="slide 2" /></a>  
            <a href="#"><img src="<?php echo base_url(); ?>images/slider/img_3_blank.jpg" alt="slide 3" /></a> 	        	
	
    	</div>
		<div class="templatemo_caption">
                    <b><h3>Bienvenido</h3></b>
		</div> 
		
  </header>
  	<div class="templatemo_white" id="templatemo_contact">
	<div class="templatemo_paracenter">
    	<h2></h2></div>
		<div class="templatemo_paracenter">
    	<h2></h2></div>
        <div class="clear"></div>
	<div class="templatemo_reasonbg"></div>
	</div>
	
	
	<div class="templatemo_lightgrey_about" id="templatemo_about">
	
	<div class="clear"></div>
	<div class="container">
    	<div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12">
        	<div class="item project-post">
						<div class="templatemo_about_box">
                        	<div class="square_coner">
                     		   <span class="texts-a"><i class="fa fa-home"></i></span>
                      	  </div>                          
                     	   Temperatura Interna: <?php echo $sensores[0]->temp; ?>°c</div>
						<div class="col-xs-12 col-sm-6 col-md-3 hover-box" >
							<div class="inner-hover-box">								
								<p>La temperatura ideal para que tu planta crezca fuerte y sana está entre 23°c y 25°c.</p>
							</div>
						</div>
					</div> 	
        </div>
        <div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12">
        	<div class="item project-post">
						<div class="templatemo_about_box">
                        	<div class="square_coner">
                     		   <span class="texts-a"><i class="fa fa-globe"></i></span>
                      	  </div>
                     	   Temperatura Externa: <?php echo $sensores[0]->temp1; ?>°c</div>
						<div class="col-xs-6 col-sm-6 col-md-3 hover-box" >
							<div class="inner-hover-box">								
								<p>La temperatura ideal para que tu planta crezca fuerte y sana está entre 23°c y 25°c.</p>
							</div>
						</div>
					</div> 	
        </div>
        <div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12 templatemo_margintop10">
        	<div class="item project-post" >
						<div class="templatemo_about_box">
                        	<div class="square_coner">
                     		   <span class="texts-a"><i class="fa fa-tint"></i></span>
                      	  </div>
                     	   Humedad: <?php echo $sensores[0]->humedad; ?>%</div>
						<div class="col-xs-6 col-sm-6 col-md-3 hover-box">
							<div class="inner-hover-box">								
								<p>Para evitar plagas o un mal crecimiento de la planta, la humedad ideal esta entre el 68% y 72%.</p>
							</div>
						</div>
					</div> 	
        </div>
        <div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12 templatemo_margintop10">
        	<div class="item project-post">
						<div class="templatemo_about_box">
                        	<div class="square_coner">
                     		   <span class="texts-a"><i class="fa fa-power-off"></i></span>
                      	  </div>
                     	   Riego automatico :<?php $estado = $sensores[0]->bomba; if ($estado) {
                              echo 'Encendido';
                           }else{echo 'Apagado';}?></div>
						<div class="col-xs-6 col-sm-6 col-md-3 hover-box"   id="bombabtn">
							<div class="inner-hover-box">								
								<p>Pulse este boton para <strong><?php $estado = $sensores[0]->bomba; if ($estado) {
                              echo 'desactivar';
                           }else{echo 'activar';}?></strong> el riego automatico de su cultivo.</p>
							</div>
						</div>
					</div> 	
        </div>
    </div>
    </div>

    <div class="clear"></div>
    <div class="templatemo_reasonbg">
    	<h2>Cultivo Hidroponico V2.0</h2>
       		<p>Si se siembra la semilla con fe y se cuida con perseverancia, sólo será cuestión de tiempo recoger sus frutos.</p>
            <div class="clear height10"></div>
	        <div class="fa fa-leaf"></div>
			<div class="clear height20"></div>
    </div>
    <div class="clear"></div>
    
    
	
	
	
	
    
    
	
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- <script src="https://code.jquery.com/jquery.js"></script> -->
    <script src="<?php echo base_url(); ?>js/jquery-1.10.2.min.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.cookie.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.cycle2.min.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.cycle2.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.nivo.slider.pack.js"></script>
    <script>$.fn.cycle.defaults.autoSelector = '.slideshow';</script>
    <script type="text/javascript">
      $(function(){
          var default_view = 'grid';
          if($.cookie('view') !== 'undefined'){
              $.cookie('view', default_view, { expires: 7, path: '/' });
          } 
          function get_grid(){
              $('.list').removeClass('list-active');
              $('.grid').addClass('grid-active');
              $('.prod-cnt').animate({opacity:0},function(){
                  $('.prod-cnt').removeClass('dbox-list');
                  $('.prod-cnt').addClass('dbox');
                  $('.prod-cnt').stop().animate({opacity:1});
              });
          }
          function get_list(){
              $('.grid').removeClass('grid-active');
              $('.list').addClass('list-active');
              $('.prod-cnt').animate({opacity:0},function(){
                  $('.prod-cnt').removeClass('dbox');
                  $('.prod-cnt').addClass('dbox-list');
                  $('.prod-cnt').stop().animate({opacity:1});
              });
          }
          if($.cookie('view') == 'list'){ 
              $('.grid').removeClass('grid-active');
              $('.list').addClass('list-active');
              $('.prod-cnt').animate({opacity:0});
              $('.prod-cnt').removeClass('dbox');
              $('.prod-cnt').addClass('dbox-list');
              $('.prod-cnt').stop().animate({opacity:1}); 
          } 

          if($.cookie('view') == 'grid'){ 
              $('.list').removeClass('list-active');
              $('.grid').addClass('grid-active');
              $('.prod-cnt').animate({opacity:0});
                  $('.prod-cnt').removeClass('dboxlist');
                  $('.prod-cnt').addClass('dbox');
                  $('.prod-cnt').stop().animate({opacity:1});
          }

          $('#list').click(function(){   
              $.cookie('view', 'list'); 
              get_list()
          });

          $('#grid').click(function(){ 
              $.cookie('view', 'grid'); 
              get_grid();
          });

          /* filter */
          $('.menuSwitch ul li').click(function(){
              var CategoryID = $(this).attr('category');
              $('.menuSwitch ul li').removeClass('cat-active');
              $(this).addClass('cat-active');
              
              $('.prod-cnt').each(function(){
                  if(($(this).hasClass(CategoryID)) == false){
                     $(this).css({'display':'none'});
                  };
              });
              $('.'+CategoryID).fadeIn(); 
              
          });
      });
    </script>
	<script src="<?php echo base_url(); ?>js/jquery.singlePageNav.js"></script>
	
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider({
          prevText: '',
          nextText: '',
          controlNav: false,
        });
    });
    </script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();
        
        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
      </script>
      <script type="text/javascript">
      <!--
          function toggle_visibility(id) {
             var e = document.getElementById(id);
             if(e.style.display == 'block'){
                e.style.display = 'none';
                $('#togg').text('show footer');
            }
             else {
                e.style.display = 'block';
                $('#togg').text('hide footer');
            }
          }
      //-->
      </script>
      
      <script type="text/javascript">
      $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
          if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
              $('html,body').animate({
                scrollTop: target.offset().top
              }, 1000);
              return false;
            }
          }
        });
      });
      </script>
      <script src="<?php echo base_url(); ?>js/stickUp.min.js" type="text/javascript"></script>
      <script type="text/javascript">
        //initiating jQuery
        jQuery(function($) {
          $(document).ready( function() {
            //enabling stickUp on the '.navbar-wrapper' class
            $('.mWrapper').stickUp();
          });
        });
      </script>
      <script>
     $('a.menu').click(function(){
    $('a.menu').removeClass("active");
    $(this).addClass("active");
	});
      </script>
      
      <script> <!-- scroll to specific id when click on menu -->
      	 // Cache selectors
var lastId,
    topMenu = $("#top-menu"),
    topMenuHeight = topMenu.outerHeight()+15,
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
      var item = $($(this).attr("href"));
      if (item.length) { return item; }
    });

// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function(e){
  var href = $(this).attr("href"),
      offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
  $('html, body').stop().animate({ 
      scrollTop: offsetTop
  }, 300);
  e.preventDefault();
});

// Bind to scroll
$(window).scroll(function(){
   // Get container scroll position
   var fromTop = $(this).scrollTop()+topMenuHeight;
   
   // Get id of current scroll item
   var cur = scrollItems.map(function(){
     if ($(this).offset().top < fromTop)
       return this;
   });
   // Get the id of the current element
   cur = cur[cur.length-1];
   var id = cur && cur.length ? cur[0].id : "";
   
   if (lastId !== id) {
       lastId = id;
       // Set/remove active class
       menuItems
         .parent().removeClass("active")
         .end().filter("[href=#"+id+"]").parent().addClass("active");
   }                   
});
      </script>

 
</body>
</html>